package start;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import basic.Logger;
import basic.PropertiesReader;
import basic.WorkingData;
import format.Beautifier;
import format.ProLine;

public class Start {

	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("No input arguments! Need name of file with list to process!");
			System.exit(-1);
		}
		System.setProperty("line.separator", "\n");

		// Read Properties file.
		boolean doAssignSector = false;
		boolean doIndentation = false;
		boolean doCommas = false;
		boolean doIndentationSelect = false;
		boolean replaceTabs = false;
		try {
			// Load debug Properties and set Logger
			boolean createDebugFile = PropertiesReader.getValue("CreateDebugFile");
			if (createDebugFile) {
				Logger.info = true;
				Logger.info_detail = false;
			}
			Logger.printInfoStart("PL SQL formatter");
			Logger.printInfo("CreateDebugFile  : " + createDebugFile);

			// Load other Properties
			doAssignSector = PropertiesReader.getValue("AssignSector");
			Logger.printInfo("AssignSector     : " + doAssignSector);
			doIndentation = PropertiesReader.getValue("Indentation");
			Logger.printInfo("Indentation      : " + doIndentation);
			doCommas = PropertiesReader.getValue("MoveCommas");
			Logger.printInfo("MoveCommas       : " + doCommas);
			doIndentationSelect = PropertiesReader.getValue("IndentationSelect");
			Logger.printInfo("IndentationSelect: " + doIndentationSelect);
			replaceTabs = PropertiesReader.getValue("ReplaceTabs");
			Logger.printInfo("ReplaceTabs      : " + replaceTabs);
		} catch (IOException ioEx) {
			Logger.printError(ioEx.toString());
			System.exit(-1);
		}
		ProLine.replaceTabs = replaceTabs;

		// Read data from arguments files.
		String gitAuthor = "Anonymous";
		List<String> listPackageBody = null;
		try {
			Logger.printInfoStart("Reading *.pkb files to process from file: '" + args[0] + "'");
			listPackageBody = WorkingData.getChangesFromFile(args[0]);
			Logger.printInfoEnd("Changes ready.");

			if (args.length > 1) {
				Logger.printInfoStart("Reading author from file: '" + args[1] + "'");
				gitAuthor = WorkingData.getAuthorFromFile(args[1]);
				Logger.printInfoEnd("Author ready.");
			}
		} catch (IOException ioEx) {
			Logger.printError(ioEx.toString());
			System.exit(-1);
		}

		// Go through all package body files.
		Iterator<String> iterator = listPackageBody.iterator();
		while (iterator.hasNext()) {
			String fullToRead = iterator.next();
			Logger.printInfoStart("Working on file: '" + fullToRead + "'");
			String fileName = fullToRead.substring(fullToRead.lastIndexOf("/") + 1);
			Logger.printInfo("File name: '" + fileName + "'");
			try {

				Beautifier beautifier = new Beautifier(fullToRead, fileName);
				beautifier.beautify(doAssignSector, doIndentation, doIndentationSelect, doCommas, gitAuthor);

			} catch (IOException ioEx) {
				Logger.printError(ioEx.toString());
				System.exit(-1);
			}
			Logger.printInfoEnd("File: '" + fileName + "' generated successfully.");
		} // all files
		Logger.printInfoEnd("All Formatted successfully.");
	}
}