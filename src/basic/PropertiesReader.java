package basic;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

	private static boolean isInitialized = false;
	private static Properties properties = null;

	/**
	 * Return single property for input String.
	 *
	 * @param property Property to get from *.properties file.
	 * @return Value of given property.
	 * @throws IOException
	 */
	public static boolean getValue(String property) throws IOException {
		if (!PropertiesReader.isInitialized) PropertiesReader.init();
		String sProperty = PropertiesReader.properties.getProperty(property);
		boolean bProperty = Boolean.valueOf(sProperty);
		return bProperty;
	}

	/**
	 * Load the properties file. (Initialize class.)
	 *
	 * @throws IOException
	 */
	private static void init() throws IOException {
		PropertiesReader.properties = new Properties();
		PropertiesReader.properties.load(new FileInputStream("constants.properties"));
		PropertiesReader.isInitialized = true;
	}

}
