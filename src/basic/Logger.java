package basic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Logger {

	// What to Debug.
	public static boolean info = false;
	public static boolean info_detail = false;

	// Is initialized?
	private static boolean isIinitialized = false;

	// Where to store.
	private static Writer writer = null;

	// Constants for printing.
	private static final String FIRST = "";
	private static String spaceAdd = "";
	private static int spaceAddCounter = 0;

	public static void printError(String toPrint) {
		Logger.printMessage(Logger.FIRST + Logger.spaceAdd + "[ERROR]" + " " + toPrint);
	}

	public static void printInfo(String toPrint) {
		if (Logger.info) {
			Logger.printMessage(Logger.FIRST + Logger.spaceAdd + "[INFO] " + " " + toPrint);
		}
	}

	public static void printInfoDetail(String toPrint) {
		if (Logger.info_detail) {
			Logger.printMessage(Logger.FIRST + Logger.spaceAdd + "[INFO+]" + " " + toPrint);
		}
	}

	public static void printInfoStart(String toPrint) {
		if (Logger.info) {
			Logger.printMessage(Logger.FIRST + Logger.spaceAdd + "[START]" + " " + toPrint);
			Logger.changeSpaceAdd(true);
		}
	}

	public static void printInfoEnd(String toPrint) {
		if (Logger.info) {
			Logger.changeSpaceAdd(false);
			Logger.printMessage(Logger.FIRST + Logger.spaceAdd + "[END]  " + " " + toPrint);
		}
	}

	public static void init() {
		try {
			File outputFile = new File("debug.log");
			Logger.writer = new BufferedWriter(new FileWriter(outputFile));
		} catch (IOException ioEx) {
			System.out.println(ioEx.toString());
			System.exit(-1);
		}
		Logger.isIinitialized = true;
	}

	public static void finish() {
		if (Logger.isIinitialized) {
			try {
				Logger.writer.close();
			} catch (IOException ioEx) {
				System.out.println(ioEx.toString());
				System.exit(-1);
			}
		}
	}

	private static void printMessage(String message) {
		if (!Logger.isIinitialized) {
			Logger.init();
		}
		try {
			Logger.writer.write(message + "\n");
			Logger.writer.flush();
		} catch (IOException ioEx) {
			System.out.println(ioEx.toString());
			System.exit(-1);
		}
	}

	private static void changeSpaceAdd(boolean plusMinus) {
		if (plusMinus) {
			Logger.spaceAddCounter++;
		} else {
			Logger.spaceAddCounter--;
		}

		Logger.spaceAdd = "";
		for (int i = 0; i < Logger.spaceAddCounter; i++) {
			Logger.spaceAdd = Logger.spaceAdd + "\t";
		}
	}
}