package basic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WorkingData {

	/**
	 * Read list of specific (*.pkb) changes from input file.
	 *
	 * @param fileName name of file to read from
	 * @return list of changed *.pkb
	 */
	public static List<String> getChangesFromFile(String fileName) throws IOException {
		List<String> listPackageBody = new ArrayList<>();

		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line = reader.readLine();
		while (line != null) {
			line = line.trim();
			if (line.endsWith(".pkb")) {
				listPackageBody.add(line);
			}
			line = reader.readLine();
		}
		reader.close();

		return listPackageBody;
	}

	/**
	 * Reads author from input file.
	 *
	 * @param fileName name of file to read author from
	 * @return author name in file
	 * @throws IOException if file not found
	 */
	public static String getAuthorFromFile(String fileName) throws IOException {
		String author = "No author data!";

		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line = reader.readLine();
		if (line != null) {
			author = (line.substring(0, line.indexOf("<"))).trim();
		}
		reader.close();

		return author;
	}

}