package format;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Complete work with PL/SQL line.
 * Separate indentation, code and comments.
 * Remove trailing white space. Remove alien junk.
 */
public class ProLine {

	public static boolean replaceTabs = false;

	private String indentation = "";
	private String code = "";
	private String codeNoString = "";
	private String comment = "";
	private boolean isEmpty = false;
	private boolean isCommaStart = false;
	private int parentheses = 0;

	/**
	 * ProLine constructor. Construct new ProLine.
	 *
	 * @param line String to split and process
	 */
	public ProLine(String line) {

		if (line.isEmpty()) {
			this.isEmpty = true;
		} else {

			String indentationCodeComment = line.replaceAll("\\s++$", ""); // Remove trailing whitespace.
			// Try to remove alien junk.
			String emptyLine = line.replaceAll("--__--__--", "").trim();
			if (emptyLine.isEmpty()) indentationCodeComment = emptyLine;

			if (indentationCodeComment.isEmpty()) {
				this.isEmpty = true;
			} else {
				this.isEmpty = false;

				// Indentation
				String codeComment = indentationCodeComment.trim();
				this.indentation = indentationCodeComment.substring(0, indentationCodeComment.indexOf(codeComment));

				// Code and Comments
				if (codeComment.contains("--")) {

					String codeTrailing = codeComment.substring(0, codeComment.indexOf("--"));
					this.code = codeTrailing.trim();
					this.comment = codeTrailing.substring(this.code.length()) + codeComment.substring(codeComment.indexOf("--"));
					if (this.code.isEmpty()) {
						this.comment = this.comment.trim();
					}
					if (this.comment.trim().equals("--")) {
						this.comment = "";
						if (this.code.isEmpty()) {
							this.indentation = "";
							this.isEmpty = true;
						}
					}

				} else if (codeComment.contains("/*")) {

					String codeTrailing = codeComment.substring(0, codeComment.indexOf("/*"));
					this.code = codeTrailing.trim();
					this.comment = codeTrailing.substring(this.code.length()) + codeComment.substring(codeComment.indexOf("/*"));

				} else {
					this.code = codeComment;
					this.comment = "";
				}

				if (!this.code.isEmpty()) {
					// Count parentheses, remove tabs, arrange commas and logical '('
					this.parentheses = 0;
					boolean apostropheSector = false;
					boolean quotationSector = false;
					String newCode = "";
					String white = "";
					boolean addWhite = true;
					for (int i = 0; i < this.code.length(); i++) {
						char c = this.code.charAt(i);
						if (c == '\'') apostropheSector = !apostropheSector;
						if (c == '"') quotationSector = !quotationSector;
						if ((c == '"') || (c == '\'')) {
							if (addWhite) newCode += white;
							newCode += c;
							white = "";
							addWhite = true;
						} else if (apostropheSector || quotationSector) {
							newCode += c;
						} else {
							this.codeNoString += c;
							// not TEXT sector
							if ((c == '\t') || (c == ' ')) {
								if (ProLine.replaceTabs) {
									white += " ";
								} else {
									white += c;
								}
							} else {
								boolean addOneSpaceAfter = false;
								if (c == ',') {
									addOneSpaceAfter = true;
									addWhite = false;
								} else if (c == ';') {
									addWhite = false;
								} else if (c == '(') {
									this.parentheses++;
									if (i == (this.code.length() - 1)) {
										newCode += " ";
									} else {
										if (addWhite) newCode += white;
									}
									addWhite = false;
								} else if (c == ')') {
									this.parentheses--;
									addWhite = true;
								} else {
									if (addWhite) newCode += white;
									addWhite = true;
								}
								newCode += c;
								if (addOneSpaceAfter) newCode += " ";
								white = "";
							}
						}
					}
					this.code = newCode.trim();

					// Formatting "||"
					if (this.code.contains("||")) {
						String code = this.code;
						String niceCode = "";
						boolean addEnd = false;
						if (code.startsWith("||")) {
							niceCode += "|| ";
							code = code.substring(2).trim();
						}
						if (code.endsWith("||")) {
							addEnd = true;
							code = code.substring(0, code.length() - 2).trim();
						}
						String[] arrayString = code.split("\\|\\|");
						for (int i = 0; i < arrayString.length; i++) {
							niceCode += arrayString[i].trim();
							if (i < arrayString.length - 1) niceCode += " || ";
						}
						niceCode = niceCode.trim();
						if (addEnd) niceCode += " ||";
						this.code = niceCode;
					}

					// Formatting ":=" (no sector)
					if (this.code.contains(":=") && (!this.code.endsWith(";"))) {
						String niceCode = "";
						String[] arrayString = this.code.split(":=");
						for (int i = 0; i < arrayString.length; i++) {
							niceCode += arrayString[i].trim();
							if (i < arrayString.length - 1) niceCode += " := ";
						}
						this.code = niceCode.trim();
					}

					// Formatting "=>" (no sector)
					if (this.code.contains("=>") && (this.code.endsWith(";"))) {
						String niceCode = "";
						String[] arrayString = this.code.split("=>");
						for (int i = 0; i < arrayString.length; i++) {
							niceCode += arrayString[i].trim();
							if (i < arrayString.length - 1) niceCode += " => ";
						}
						this.code = niceCode.trim();
					}

					if (ProLine.replaceTabs) this.comment = this.comment.replace('\t', ' ');
					// Check start comma.
					if (this.code.startsWith(",")) {
						this.isCommaStart = true;
					}
				} // code not empty
			} // line not empty
		} // line not empty

	}

	/**
	 * Splits code based on last parentehese.
	 *
	 * @return splitted code as list
	 */
	public List<String> parentheseCodeSplitter() {
		List<String> codeList = new ArrayList<>();
		String sCode = this.code;

		if (this.parentheses > 0) {

			for (int i = 0; i < Math.abs(this.parentheses); i++) {
				int position = sCode.indexOf("(");
				if ((position + 1) < sCode.length()) {
					codeList.add(sCode.substring(0, position + 1));
					sCode = sCode.substring(position + 1).trim();
				}
			}
			codeList.add(sCode);

		} else {

			for (int i = 0; i < Math.abs(this.parentheses); i++) {
				int position = sCode.lastIndexOf(")");
				if (position > 0) {
					codeList.add(sCode.substring(position));
					sCode = sCode.substring(0, position).trim();
				}
			}
			codeList.add(sCode);
			Collections.reverse(codeList);
		}

		return codeList;
	}

	public void addCommaEnd() {
		this.code = this.code + ",";
	}

	public void removeCommaStart() {
		this.code = this.code.substring(1).trim();
		this.isCommaStart = false;
	}

	public boolean updateAuthorTag(String author) {

		if (this.comment.contains("@author")) {
			this.comment = this.comment.substring(0, this.comment.indexOf("@author") + 7) + " " + author;
			return true;
		}
		return false;
	}

	public boolean updateVersionTag(String version) {

		if (this.comment.contains("@version")) {
			this.comment = this.comment.substring(0, this.comment.indexOf("@version") + 8) + " " + version;
			return true;
		}
		return false;
	}

	public boolean isEmpty() {
		return this.isEmpty;
	}

	public boolean isCommaStart() {
		return this.isCommaStart;
	}

	public int getParentheses() {
		return this.parentheses;
	}

	public String getLine() {
		return this.indentation + this.code + this.comment;
	}

	public String getIndentation() {
		return this.indentation;
	}

	public String getCode() {
		return this.code;
	}

	public String getCodeNoString() {
		return this.codeNoString;
	}

	public String getComment() {
		return this.comment;
	}
}
