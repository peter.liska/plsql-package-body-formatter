package format;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BeautifySectors {

	// for getters
	private boolean swallowed = false;
	private boolean sectorReady = false;
	private String sectorLines = "";

	// private properties
	private boolean sectorAssign = false;
	private List<ProLine> listSectorAssign = new ArrayList<>();
	private boolean sectorAssign2 = false;
	private List<ProLine> listSectorAssign2 = new ArrayList<>();
	boolean sectorInOut = false;

	private List<ProLine> listSectorInOut = new ArrayList<>();

	// RegEx
	private String regStart = ".*\\b(?i)";
	private String regEnd = "\\b.*";

	public void beautify(ProLine proLine) {

		this.swallowed = false;
		boolean sectorInOutNow = false;
		boolean sectorAssignNow = false;
		boolean sectorAssign2Print = false;

		String code = proLine.getCode();

		if (code.isEmpty()) {
			if (this.sectorAssign2) {
				sectorAssign2Print = true;
			}
		} else {

			// sector IN / OUT / NOCOPY
			if (this.sectorInOut) {
				if (code.matches(this.regStart + "IN" + this.regEnd)) {
					sectorInOutNow = true;
				} else if (code.matches(this.regStart + "OUT" + this.regEnd)) {
					sectorInOutNow = true;
				} else if (code.matches(this.regStart + "NOCOPY" + this.regEnd)) {
					sectorInOutNow = true;
				}
				if (sectorInOutNow) {
					this.listSectorInOut.add(proLine);
					this.swallowed = true;
				}
			} else {
				if (code.matches(this.regStart + "FUNCTION" + this.regEnd) || code.matches(this.regStart + "PROCEDURE" + this.regEnd)) {
					this.sectorInOut = true;
					sectorInOutNow = true;
				}
			}

			// sector assign 1
			if (code.contains(":=") && code.endsWith(";")) {
				sectorAssignNow = true;
				this.sectorAssign = true;
				this.listSectorAssign.add(proLine);
				this.swallowed = true;
			}

			// sector assign 2
			if (!sectorAssignNow) {
				if (code.contains("=>")) {
					this.sectorAssign2 = true;
					this.listSectorAssign2.add(proLine);
					this.swallowed = true;
					if (!code.endsWith(",")) {
						sectorAssign2Print = true;
					}
				}
			}

		} // code not empty

		// print Sectors
		this.sectorReady = false;
		this.sectorLines = "";

		if (this.sectorInOut && !sectorInOutNow) {
			if (!this.listSectorInOut.isEmpty()) {
				this.sectorLines += this.writeInOutSector(this.listSectorInOut);
				this.sectorReady = true;
			}
			this.listSectorInOut.clear();
			this.sectorInOut = false;
		}

		if (this.sectorAssign && !sectorAssignNow) {
			this.sectorLines += this.writeAssignSector(this.listSectorAssign, ":=");
			this.sectorReady = true;
			this.listSectorAssign.clear();
			this.sectorAssign = false;
		}

		if (this.sectorAssign2 && sectorAssign2Print) {
			this.sectorLines += this.writeAssignSector(this.listSectorAssign2, "=>");
			this.sectorReady = true;
			this.listSectorAssign2.clear();
			sectorAssign2Print = false;
			this.sectorAssign2 = false;
		}

	}

	/**
	 * Format and write Assign sector.
	 *
	 * @param list to work with
	 */
	private String writeAssignSector(List<ProLine> list, String assignMark) {
		// Search for longest left
		int maxLength = 0;
		Iterator<ProLine> iterator = list.iterator();
		while (iterator.hasNext()) {
			ProLine proLine = iterator.next();
			String tmpCode = proLine.getCode();
			int length = tmpCode.substring(0, tmpCode.indexOf(assignMark)).trim().length();
			if (maxLength < length) maxLength = length;
		}

		// Write it nice
		String returnString = "";

		for (int i = 0; i < list.size(); i++) {

			ProLine proLine = list.get(i);
			String tmpCode = proLine.getCode();
			String codeStart = tmpCode.substring(0, tmpCode.indexOf(assignMark)).trim();
			String codeEnd = tmpCode.substring(tmpCode.indexOf(assignMark) + 2).trim();
			int length = codeStart.length();
			if (length < maxLength) {
				for (int j = length; j < maxLength; j++) {
					codeStart = codeStart + " ";
				}
			}

			String niceCode = codeStart + " " + assignMark + " " + codeEnd;
			returnString += proLine.getIndentation() + niceCode + proLine.getComment();
			returnString += "\n";
		}

		return returnString;
	}

	/**
	 * Format and write IN/OUT/ sector.
	 *
	 * @param list to work with
	 */
	private String writeInOutSector(List<ProLine> list) {

		int maxLength = 0;
		List<String> listStart = new ArrayList<>();
		List<String> listEnd = new ArrayList<>();

		boolean isIn = false;
		boolean isOut = false;
		boolean isNocopy = false;
		List<Boolean> listIn = new ArrayList<>();
		List<Boolean> listOut = new ArrayList<>();
		List<Boolean> listNocopy = new ArrayList<>();

		// Prepare lists
		Iterator<ProLine> iterator = list.iterator();
		while (iterator.hasNext()) {
			ProLine proLine = iterator.next();
			String code = proLine.getCode();
			String startWord = "";
			boolean startWordDone = false;

			for (int i = 0; i < 3; i++) {
				String check = "IN";
				if (i == 1) check = "OUT";
				if (i == 2) check = "NOCOPY";

				int position = -1;
				boolean bResult = false;
				Pattern pattern = Pattern.compile("\\b" + check + "\\b");
				Matcher matcher = pattern.matcher(code.toUpperCase());
				if (matcher.find()) {
					position = matcher.start();
				}
				if (position >= 0) {
					bResult = true;
					if (!startWordDone) {
						startWord = code.substring(0, position).trim();
						startWordDone = true;
						int length = startWord.length();
						if (maxLength < length) maxLength = length;
					}
					code = code.substring(position + check.length()).trim();
				}

				if (i == 0) {
					listIn.add(bResult);
					if (bResult) isIn = bResult;
				}
				if (i == 1) {
					listOut.add(bResult);
					if (bResult) isOut = bResult;
				}
				if (i == 2) {
					listNocopy.add(bResult);
					if (bResult) isNocopy = bResult;
				}
			}
			listStart.add(startWord);
			listEnd.add(code);
		}

		// Print lists
		String returnString = "";
		for (int i = 0; i < list.size(); i++) {

			String codeStart = listStart.get(i);
			int length = codeStart.length();
			if (length < maxLength) {
				for (int j = length; j < maxLength; j++) {
					codeStart = codeStart + " ";
				}
			}

			String niceCode = codeStart + "  ";
			if (isIn) {
				if (listIn.get(i)) {
					niceCode += "IN ";
				} else {
					niceCode += "   ";
				}
			}
			if (isOut) {
				if (listOut.get(i)) {
					niceCode += "OUT ";
				} else {
					niceCode += "    ";
				}
			}
			if (isNocopy) {
				if (listNocopy.get(i)) {
					niceCode += "NOCOPY ";
				} else {
					niceCode += "       ";
				}
			}
			niceCode += " " + listEnd.get(i);

			ProLine proLine = list.get(i);
			returnString += proLine.getIndentation() + niceCode + proLine.getComment();
			returnString += "\n";
		}

		return returnString;
	}

	public boolean isSwallowed() {
		return this.swallowed;
	}

	public boolean isSectorReady() {
		return this.sectorReady;
	}

	public String getSectorLines() {
		return this.sectorLines;
	}

}