package format;

public class Indentation {

	private int next;
	private int once;
	private String space;
	private int length;

	public Indentation() {
		this.next = 0;
		this.once = 0;
		this.space = "";
		this.length = 0;
	}

	public void add(int counter) {
		this.length = this.length + counter;
		this.space = this.generateSpace(this.length);
	}

	public void addNext(int counter) {
		this.next = counter;
	}

	public void addOnce(int counter) {
		this.once = counter;
	}

	public int getLength() {
		return this.length;
	}

	public String getSpace() {
		String returnSpace = this.space;

		if (this.once != 0) {
			returnSpace = this.generateSpace(this.length + this.once);
			this.once = 0;
		}

		if (this.next != 0) {
			this.length = this.length + this.next;
			this.space = this.generateSpace(this.length);
			this.next = 0;
		}

		return returnSpace;
	}

	/**
	 * Generates Indentation string based on given size.
	 *
	 * @param size size of indentation
	 * @return indentation string
	 */
	private String generateSpace(int size) {
		String str = "";
		for (int i = 0; i < size; i++) {
			str = str + "\t";
		}
		return str;
	}
}
