package format;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

public class BeautifyLines {

	// User Settings
	private boolean doIndentation = false;
	private boolean doIndentationSelect = false;
	private boolean doCommas = false;
	private String gitAuthor = "Dain Ironfoot";

	// Other variables
	private String actualTime = "future";
	private boolean authorDone = false;
	private boolean versionDone = false;

	// Important variables
	private Indentation indentation = new Indentation();
	private boolean removeCommaStart = false;

	// Sectors
	private boolean sectorCase = false;
	private boolean sectorCursor = false;
	private boolean sectorIs = false;
	private boolean sectorSelect = false;

	// RegEx
	private String regStart = ".*\\b(?i)";
	private String regEnd = "\\b.*";

	public BeautifyLines(boolean doIndentation, boolean doIndentationSelect, boolean doCommas, String gitAuthor) {

		this.doIndentation = doIndentation;
		this.doIndentationSelect = doIndentationSelect;
		this.doCommas = doCommas;
		this.gitAuthor = gitAuthor;

		// Set DateTime
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime localDateTimeNow = LocalDateTime.now();
		this.actualTime = dateTimeFormatter.format(localDateTimeNow);
	}

	public String beautify(ProLine proLine, ProLine proLineNext) {

		// Commas
		if (this.doCommas) {
			if (this.removeCommaStart) {
				proLine.removeCommaStart();
				this.removeCommaStart = false;
			}
			if (proLineNext != null && !proLineNext.isEmpty()) {
				if (proLineNext.isCommaStart()) {
					proLine.addCommaEnd();
					this.removeCommaStart = true;
				}
			}
		}

		// Author & Version
		if (!this.authorDone) {
			this.authorDone = proLine.updateAuthorTag(this.gitAuthor);
		}
		if (!this.versionDone) {
			this.versionDone = proLine.updateVersionTag(this.actualTime);
		}

		// if no indentation return proLine
		if (!this.doIndentation) return proLine.getLine();

		// INDENTATION -----

		if (!proLine.getCode().isEmpty()) {
			String code = proLine.getCodeNoString();

			// sector SELECT
			if (!this.sectorSelect) {
				if (this.doIndentationSelect) {
					if ((code.toUpperCase().startsWith("SELECT") || code.toUpperCase().startsWith("UPDATE")) && (!code.endsWith(";"))) {
						this.sectorSelect = true;
						this.indentation.addNext(1);
					}
				}
			}
			if (this.sectorSelect) {
				if (code.toUpperCase().startsWith("FROM")) this.indentation.addOnce(-1);
				if (code.toUpperCase().startsWith("SET")) this.indentation.addOnce(-1);
				if (code.toUpperCase().startsWith("WHERE")) this.indentation.addOnce(-1);
				if (code.toUpperCase().startsWith("ORDER BY")) this.indentation.addOnce(-1);
				if (code.toUpperCase().startsWith("MINUS")) {
					this.sectorSelect = false;
					this.indentation.add(-1);
				}
				if (code.endsWith(";")) {
					this.sectorSelect = false;
					this.indentation.addNext(-1);
				}
			} else {
				// not sector SELECT

				// CURSOR
				if (code.matches(this.regStart + "CURSOR" + this.regEnd)) {
					this.sectorCursor = true;
				}
				if (this.sectorCursor) {
					if (code.endsWith(";")) this.sectorCursor = false;
				} else {
					// IS
					if (code.toUpperCase().equals("IS")) {
						this.sectorIs = true;
						this.indentation.addNext(1);
					}
				}

				// CREATE
				if (code.matches(this.regStart + "CREATE" + this.regEnd)) {
					this.indentation.addNext(1);
				}
				// BEGIN
				if (code.matches(this.regStart + "BEGIN" + this.regEnd)) {
					if (this.sectorIs) {
						this.sectorIs = false;
						this.indentation.addOnce(-1);
					} else {
						this.indentation.addNext(1);
					}
				}
				// LOOP
				if (code.matches(this.regStart + "LOOP" + this.regEnd)) {
					if (!code.matches(this.regStart + "END" + this.regStart + "LOOP" + this.regEnd)) {
						this.indentation.addNext(1);
					}
				}
				// CASE
				if (code.matches(this.regStart + "CASE" + this.regEnd)) {
					this.indentation.addNext(1);
					this.sectorCase = true;
				}
				if (!this.sectorCase) {
					// THEN
					if (code.matches(this.regStart + "THEN" + this.regEnd)) {
						this.indentation.addNext(1);
					}
					// WHEN
					if (code.matches(this.regStart + "WHEN" + this.regEnd)) {
						this.indentation.add(-1);
					}
				}
				// EXCEPTION
				if (code.matches(this.regStart + "EXCEPTION" + this.regEnd)) {
					if (!code.endsWith(";")) {
						this.indentation.addOnce(-1);
					}
				}
				// ELSE + ELSIF
				if (code.matches(this.regStart + "ELSE" + this.regEnd)) this.indentation.addOnce(-1);
				if (code.matches(this.regStart + "ELSIF" + this.regEnd)) this.indentation.add(-1);
				// END
				if (code.matches(this.regStart + "END" + this.regEnd)) {
					this.indentation.add(-1);
					this.sectorCase = false;
					this.sectorIs = false;
				}
			} // not sector SELECT
		} // code not empty

		String returnLine = "";

		if (proLine.getParentheses() == 0) {
			returnLine = this.indentation.getSpace() + proLine.getCode() + proLine.getComment();
		} else {
			List<String> codeList = proLine.parentheseCodeSplitter();
			if (codeList.size() < 2) {
				if (proLine.getParentheses() > 0) this.indentation.addNext(1);
				if (proLine.getParentheses() < 0) this.indentation.add(-1);
				returnLine = this.indentation.getSpace() + proLine.getCode() + proLine.getComment();
			} else { // Parentheses more
				Iterator<String> iterator = codeList.iterator();
				int lineNumber = 1;
				while (iterator.hasNext()) {
					if (lineNumber == 1) { // first
						returnLine += this.indentation.getSpace() + iterator.next();
						if (proLine.getParentheses() < 0) returnLine += proLine.getComment();
						returnLine += "\n";
					} else if (lineNumber == codeList.size()) { // last
						if (proLine.getParentheses() < 0) this.indentation.add(-1);
						if (proLine.getParentheses() > 0) this.indentation.add(1);
						returnLine += this.indentation.getSpace() + iterator.next();
						if (proLine.getParentheses() > 0) returnLine += proLine.getComment();

					} else {
						if (proLine.getParentheses() < 0) this.indentation.add(-1);
						if (proLine.getParentheses() > 0) this.indentation.add(1);
						returnLine += this.indentation.getSpace() + iterator.next();
						returnLine += "\n";
					}
					lineNumber++;
				} // iterator
			} // Parentheses with more lines effect
		} // Parentheses

		return returnLine;
	}
}
