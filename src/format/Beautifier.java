package format;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import basic.Logger;

public class Beautifier {

	private String readPath = null;
	private String writePath = null;
	private BufferedReader reader = null;
	private BufferedWriter writer = null;

	/**
	 * Creates new Beautifier.
	 *
	 * @param readPath  path to read
	 * @param writePath Path to write
	 */
	public Beautifier(String readPath, String writePath) {
		// Set paths for reading and writing files.
		this.readPath = readPath;
		this.writePath = writePath;
	}

	/**
	 * Beautify all lines in file.
	 *
	 * @param sectors set true if you want to do sectors too
	 * @throws IOException
	 */
	public void beautify(boolean sectors, boolean doIndentation, boolean doIndentationSelect, boolean doCommas, String gitAuthor) throws IOException {

		for (int belle = 0; belle < 2; belle++) {
			if ((belle == 0) || ((belle == 1) && (sectors))) {
				if (belle == 0) {
					Logger.printInfoStart("Processing:  LINES");
				} else {
					Logger.printInfoStart("Processing:  SECTORS");
				}
				this.startFiles();

				// Initialize basic properties.
				boolean sectorNotFormat = false;
				boolean sectorComment = false;
				BeautifyLines beautifyLines = new BeautifyLines(doIndentation, doIndentationSelect, doCommas, gitAuthor);
				BeautifySectors beautifySectors = new BeautifySectors();

				// All lines - loop whole file.
				String line = this.reader.readLine();
				Logger.printInfoDetail("line> " + line);

				while (line != null) {

					// Read NEXT line.
					String lineNext = this.reader.readLine();
					ProLine proLineNext = null;
					if (lineNext != null) proLineNext = new ProLine(lineNext);

					if (!sectorNotFormat) {
						if (line.matches(".*\\/\\*\\*\\s*@formatter:off.*") || line.matches(".*\\/\\*\\s*@formatter:off.*") || line.matches(".*\\-\\-\\s*@formatter:off.*")) {
							sectorNotFormat = true;
						}
					}
					if (sectorNotFormat) {
						if (line.matches(".*\\/\\*\\*\\s*@formatter:on.*") || line.matches(".*\\/\\*\\s*@formatter:on.*") || line.matches(".*\\-\\-\\s*@formatter:on.*")) {
							sectorNotFormat = false;
						}
						this.writer.write(line);
						if (sectorNotFormat) {
							this.writer.newLine();
						} else {
							if (proLineNext != null && !proLineNext.isEmpty()) {
								this.writer.newLine();
							}
						}
					} else {
						if (!sectorComment) {
							if (line.contains("/*") && !line.contains("*/")) {
								sectorComment = true;
							}
						}
						if (sectorComment) {
							if (line.contains("*/")) {
								sectorComment = false;
							}
							this.writer.write(line);
							if (sectorComment) {
								this.writer.newLine();
							} else {
								if (proLineNext != null && !proLineNext.isEmpty()) {
									this.writer.newLine();
								}
							}
						} else {
							ProLine proLine = new ProLine(line);

							if (belle == 1) {
								beautifySectors.beautify(proLine);
								if (beautifySectors.isSectorReady()) {
									this.writer.write(beautifySectors.getSectorLines());
								}
							}

							if ((belle == 0) || (belle == 1 && !beautifySectors.isSwallowed())) {
								if (!proLine.isEmpty()) {
									if (belle == 0) {
										this.writer.write(beautifyLines.beautify(proLine, proLineNext));
									} else {
										this.writer.write(proLine.getLine());
									}
									if (proLineNext != null) {
										this.writer.newLine();
									}
								} else {
									if (proLineNext != null && !proLineNext.isEmpty()) {
										this.writer.newLine();
									}
								}
							} // belle == 0 OR not Swallowed

						} // sector Comments
					} // sector formatter:off

					// NEXT line
					if (lineNext != null) {
						Logger.printInfoDetail("line> " + lineNext);
					}
					this.writer.flush();
					line = lineNext;
				} // all file lines

				Logger.printInfo("File generated.");
				this.finishFiles();
				Logger.printInfoEnd("Finished");
			}
		}
	}

	/**
	 * Prepare files to read and write.
	 *
	 * @throws IOException
	 */
	private void startFiles() throws IOException {
		// Prepare files to read and write.
		this.reader = new BufferedReader(new FileReader(this.readPath));
		this.writer = new BufferedWriter(new FileWriter(this.writePath));
	}

	/**
	 * Close files. Delete old file and replace with new one.
	 *
	 * @throws IOException
	 */
	private void finishFiles() throws IOException {
		// Close files.
		this.reader.close();
		this.writer.close();

		// Overwrite trash file with nice file.
		File trashFile = new File(this.readPath);
		boolean deleteDone = trashFile.delete();
		Logger.printInfo("Old file deleted? " + deleteDone);
		File niceFile = new File(this.writePath);
		File newFile = new File(this.readPath);
		boolean moveDone = niceFile.renameTo(newFile);
		Logger.printInfo("File moved? " + moveDone);
	}

}
